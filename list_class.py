k = [1,2, "hi", "bye"]

# print(type(k))
#
# print(dir(k))
#
# # append
#
# k.append("kuna")
#
# print(k)
#
# # clear
#
# k.clear()
#
# print(k)

# # memory reference
# print("the orginal list is", k)
#
# a = k
#
# print("now the a list is", a)
#
# a[-1] = "joy"
#
# print("transformed a is", a)
#
# print("orginal k", k)


# # copy
#
# print("the orginal list is", k)
#
# a = k.copy()
#
# print("now the a list is", a)
#
# a[-1] = "joy"
#
# print("transformed a is", a)
#
# print("orginal k", k)

#
# # count
#
# print(k.count("bye"))

# # extend
#
# a = [4,5,6]
#
# # k.extend(a)
# #
# # print("this is extend",k)
#
# k.append(a)
#
# print("append", k)

# # index
#
# print(k.index("bye"))

# # insert
# print("original", k)
#
# k.insert(2, "kuna")
#
# print("insert", k)


# # pop
#
# print(k)
# last_element = k.pop()
#
# print(last_element)
# print(k)

# # remove
#
# k.remove("bye")
#
# print(k)



