
k = [1,2,3,4,5,6]
print(k)

#for loop is used for iteration on the data structure

# for each_element in k:
#     print(each_element) #identation 4 spaces
#
# print("loop completed")


# for x in k:
#     h = x + 10
#     print(h)

# # tips
# # built in keywords & functions
# #assignment refer to ^
#
# k = ["hi", "bye", "onemore", "extra"]
# print(len(k))
#
# #range
#
# print(list(range(0,10)))
#
# print(list(range(0, len(k))))
#
# for each_index in range(0, len(k)):
#     print(k[each_index])
#


# # if statement
#
# k = 6
#
# if (k == 6):
#     print("yes")
# else:
#     print("no")

# for each_element in k:
#     if each_element > 3:
#         print(each_element)


# # . Write a Python program to remove duplicates from a list.
#
# k = [1,2,3,3,1,4,4,4,5,6,6,7,7]
#
# d = []
# for each_ele in k:
#    for each_count in range(1, k.count(each_ele)):
#        k.remove(each_ele)
#
# print("non dup list", k)


# # while loop # untill it becomes false
#
# k = [1,2,3,4,5]
# i = 0
# while i < 3:
#     print(k[i])
#     i += 1
#     print("i is", i)

#break and continue

# k = [1,2,3,4,5,6,7,8]
#
# # for each_ele in k:
# #     if (each_ele == 4):
# #         break
# #     print(each_ele)
#
# #
# # for each_ele in k:
# #     if (each_ele == 4) or  (each_ele == 5) :
# #         continue
# #     print(each_ele)












