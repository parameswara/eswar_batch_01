# # tuple immutable
#
# k = (1,2,3)
#
# print(type(k))
#
# print(k[0])
#

# # dict
#
# h = {}
#
# print(type(h))
#
# #switch case  --- dict  key-value pair
#
# h = {1: "hi", 2: "bye", "one_more": "i am key"}
#
# print(h)
#
# print(h[1])
#
# print(h["one_more"])
#
# # keys are unique
#
# h = {1: "hi", 2: "bye", "one_more": "i am key"}
#
# print(h["one_more"])







