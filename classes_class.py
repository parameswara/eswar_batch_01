#class

# k = 5
# print(dir(k))

# class kuna():
#     def __init__(self):
#         pass
#
#     def ten_add(self, a):
#         return a + 10
#
#
# h = kuna()
# result = h.ten_add(3)
# result2 = h.ten_add(5)
# print(result)
# print(result2)


# class kuna():
#     def __init__(self, a):
#         self.a = a
#
#     def ten_add(self):
#         return self.a + 10
#
#
# h = kuna(3)
# result = h.ten_add()
# print(result)
#
# h = kuna(5)
# result = h.ten_add()
# print(result)
