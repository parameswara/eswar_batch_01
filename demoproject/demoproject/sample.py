from django.http import HttpResponse
from django.shortcuts import render
from cart.models import book, testdb


def ret_out(request):
    return HttpResponse("Hello World")


def get_request(request):
    if request.method == "GET":
        return HttpResponse("get method")
    if request.method == "POST":
        dict_data = request.POST
        name = dict_data["name"]
        gender = dict_data["gender"]
        return HttpResponse("User given data is valid {} {}".format(name, gender))

def home_page(request):
    if request.method == "POST":
        dict_data = request.POST
        name = dict_data["name"]
        data = testdb(name=name)
        data.save()
        gender = dict_data["gender"]
        default_string = "users in db are "
        db_data = testdb.objects.all()
        for each_data in db_data:
            default_string += str(each_data.name)
        return HttpResponse(default_string)
    return render(request, 'index.html')

def add_book(request):
    if request.method == "POST":
        dict_data = request.POST
        name = dict_data["name"]
        book_name = dict_data["book_name"]
        book_auth = dict_data["book_auth"]

        book1 = book(book_name=book_name, book_author=book_auth, book_pub="2001-01-01 12:00")
        book1.save()
        testdata = testdb(name=name, book_bought=book1)
        testdata.save()

        return HttpResponse("saved")






